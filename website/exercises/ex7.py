import statistics as stats
from website import app, parser

def run():
    with app.open_resource('static/input7.txt') as file:
        lines = parser.parseLineOfIntegers(file.read())
        return { "lead": "The Treachery of Whales",
                 "a": exercise_a(lines),
                 "b": exercise_b(lines)}

def exercise_a(crab_subs):

    # Median of all crabs is the least distance travelled
    median_position = stats.median(crab_subs)
    return int(sum(list(map(lambda x: abs(x-median_position), crab_subs))))


def exercise_b(crab_subs):
    min_pos = min(crab_subs)
    max_pos = max(crab_subs)

    # For each crab calculate its fuelconsumption travelling to each potential position
    # Each potential position = range(min, max)
    # Travel distance = absolute value between crab and each position in the range
    # Fuel consumed = Gauss sum (n(n+1)/2) using travel distance
    # Summarize the fuelconsumption for each crab to each potential position
    # Find the minimum fuel consumption
    return int(min([sum([calc_fuel(abs(crab_sub - target)) for crab_sub in crab_subs]) for target in range(min_pos,max_pos)]))

def calc_fuel(target):
    return (target*(target+1))/2
