from website import app, parser

def run():
    with app.open_resource('static/input2.txt') as file:
        lines = parser.parseSubmarineCommands(file.read())
        return { "lead": "Dive!",
                 "a": exerciseA(lines),
                 "b": exerciseB(lines)}

def exerciseA(lines):
    forward = sum(x[1] for x in lines if x[0] == 'forward')
    depth = sum(x[1] for x in lines if x[0] == 'down')
    return forward * depth

def exerciseB(lines):
    aim = 0
    forward = 0
    depth = 0
    for command in lines:
        if command[0] == 'forward':
            forward += command[1]
            depth += command[1] * aim
        elif command[0] == 'down':
            aim += command[1]
          
    return forward * depth






    

