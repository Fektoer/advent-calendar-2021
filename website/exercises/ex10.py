import math
from website import app, parser


def run():
    with app.open_resource('static/input10.txt') as file:
        lines = parser.parseListOfStrings(file.read())
        return {"lead": "Syntax Scoring",
                "a": exercise_a(lines),
                "b": exercise_b(lines)}


def exercise_a(lines):
    opening_characters = ['(', '[', '<', '{']
    closing_characters = [')', ']', '>', '}']
    corrupted_characters = []

    for line in lines:
        syntax = []
        characters = list(line)
        for char in characters:
            if char in opening_characters:
                syntax.append(char)
            else:
                last_character = syntax[-1:][0]
                if closing_characters.index(char) != opening_characters.index(last_character):
                    corrupted_characters.append(char)
                    break
                else:
                    syntax.pop()

    switcher = {
        ")": 3,
        "]": 57,
        "}": 1197,
        ">": 25137
    }
    return sum(list(map(switcher.get, corrupted_characters)))

def exercise_b(lines):
    opening_characters = ['(', '[', '<', '{']
    closing_characters = [')', ']', '>', '}']
    incomplete_lines = []

    for line in lines:
        is_corrupted = False
        syntax = []
        characters = list(line)
        for char in characters:
            if char in opening_characters:
                syntax.append(char)
            else:
                last_character = syntax[-1:][0]
                if closing_characters.index(char) != opening_characters.index(last_character):
                    is_corrupted = True
                    break
                else:
                    syntax.pop()
        if is_corrupted:
            continue
        incomplete_characters = list(map(lambda x: closing_characters[opening_characters.index(x)], syntax[::-1]))
        incomplete_lines.append(incomplete_characters)

    switcher = {
        ")": 1,
        "]": 2,
        "}": 3,
        ">": 4
    }

    scores = []
    for line in incomplete_lines:
        score = 0
        for character in line:
            score = score * 5 + switcher.get(character)
        scores.append(score)
    scores.sort()
    return scores[math.floor(len(scores) / 2)]
