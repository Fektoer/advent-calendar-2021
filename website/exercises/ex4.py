from website import app, parser

def run():
    with app.open_resource('static/input4.txt') as file:
        lines = parser.parseListOfStrings(file.read())

        boards = setupBoards(lines)
        drawSequence = list(lines[0].split(','))
        
        return { "lead": "Giant Squid",
                 "a": exerciseA(drawSequence, boards),
                 "b": exerciseB(drawSequence, boards)}

def exerciseA(drawSequence, boards):
    return playBingo(drawSequence, boards)
    
def exerciseB(drawSequence, boards):
    return loseBingo(drawSequence, boards)

def setupBoards(lines):
    # Parse each 5x5 board into 10 lines, 5 horizontal ones, 5 vertical ones
    # Winning a board means one of those lines needs to have 5 marked numbers

    index = 2

    # Set up the boards
    board = []
    boards = []
    while index < len(lines):

        # Add horizontal lines of the board
        row = []
        for x in list(lines[index].split()):

            # Each number is a value and a marked indicator
            row.append([int(x), False])
        board.append(row)

        # After 5 rows (and an empty row), add the vertical lines of the board
        if index % 6 == 0:
            
            tempboard = []
            for i in range(5):
                row = []
                
                for x in board:
                    row.append([int(x[i][0]), False])
                tempboard.append(row)

            board.extend(tempboard)
            boards.append(board)
            board = []
            index += 1 
        index += 1
    return boards

def playBingo(drawSequence, boards):

    for drawnNumber in drawSequence:
        drawnNumber = int(drawnNumber)

        # First mark all your bingo numbers on your boards
        for board in boards:
            for row in board:
                for number in row:
                    if number[0] == drawnNumber:
                        number[1] = True
            
            for row in board:

                # Bingo if one of the lines contains only marked values
                if all(x[1] for x in row):              
                    
                    # Since we have separate horizontal and vertical lines, we have duplicate numbers. Hence we 
                    # create a set to only get the unique unmarked values.
                    uniqueValues = set()
                    for row in board:
                        for value in row:
                            if not value[1]:
                                uniqueValues.add(value[0])

                    return sum(uniqueValues) * drawnNumber  


def loseBingo(drawSequence, boards):

    # Basically the same as win bingo, but keep a unique set of winning boards and only return the result once every board
    # has won, meaning you automatically have the last board
    fancyBoards = []
    for index, board in enumerate(boards):
        fancyBoards.append([board, index])

    winningBoards = set()

    for drawnNumber in drawSequence:
        drawnNumber = int(drawnNumber)
        for board in fancyBoards:

            # Only check for boards that have not won yet
            if board[1] not in winningBoards:
                for row in board[0]:
                    for number in row:
                        if number[0] == drawnNumber:
                            number[1] = True
                
                for row in board[0]:
                    if all(x[1] for x in row):
                        
                        winningBoards.add(board[1])

                        if len(winningBoards) == len(fancyBoards):
                            uniqueValues = set()
                            for row in board[0]:
                                for value in row:
                                    if not value[1]:
                                        uniqueValues.add(value[0])

                            return sum(uniqueValues) * drawnNumber

    

