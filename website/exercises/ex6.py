import time
from collections import deque
from website import app, parser

def run():

    with app.open_resource('static/input6.txt') as file:
        lines = parser.parseLineOfIntegers(file.read())
        return { "lead": "Lanternfish",
                 "a": exercise(lines, 80),
                 "b": exercise(lines, 256)}

def exercise(lines, days):
    start = time.time()
    # Create array of ages
    current_fishies = deque([0] * 9)

    # Set initial count of fish per age group
    for fish in lines:
        current_fishies[fish] += 1

    # Increase per day -> rotate array and create new fishies for all fishies at index 0
    for _ in range(days):
        num_new_fishies = current_fishies[0]
        current_fishies.rotate(-1)
        current_fishies[6] += num_new_fishies
    print(str((time.time()-start) * 1000) + ' ms')
    return sum(current_fishies)
