from . import ex1, ex2, ex3, ex4, ex5, ex6, ex7, ex8, ex9, ex10
import time


def run(id):
    start = time.time()
    switcher = {
        "1": ex1,
        "2": ex2,
        "3": ex3,
        "4": ex4,
        "5": ex5,
        "6": ex6,
        "7": ex7,
        "8": ex8,
        "9": ex9,
        "10": ex10
    }

    function = getattr(switcher.get(id), 'run')
    result = function()
    print('Exercise ' + id + ': ' + str((time.time()-start) * 1000) + ' ms')
    return result
