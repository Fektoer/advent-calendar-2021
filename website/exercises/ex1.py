from website import app, parser

def run():
    with app.open_resource('static/input1.txt') as file:
        lines = parser.parseListOfIntegers(file.read())
        return { "lead": "Sonar Sweep",
                 "a": exercise_a(lines),
                 "b": exercise_b(lines)}

def exercise_a(lines):
    return sum(y>x for x,y in zip(lines, lines[1:]))

def exercise_b(lines):
    return exercise_a([x+y+z for x,y,z in zip(lines, lines[1:], lines[2:])])
    