from website import app, parser

def run():
    with app.open_resource('static/input5.txt') as file:
        result = parser.parseVents(file.read())
        vent_coordinates = result['list']
        max_x = result['maxX']
        max_y = result['maxY']

        # A
        ocean_floor = define_ocean_floor(max_x, max_y)
        result_a = exercise_a(vent_coordinates, ocean_floor)

        # B
        ocean_floor = define_ocean_floor(max_x, max_y)
        result_b = exercise_b(vent_coordinates, ocean_floor)

        return { "lead": "Hydrothermal Venture",
                 "a": result_a,
                 "b": result_b }

def exercise_a(vent_coordinates, ocean_floor):
    for coordinates in vent_coordinates:

        # Create array of coordinates from point A to point B
        extended_coordinates = extend_coordinates(coordinates, False)

        # Draw Vents
        for coordinate in extended_coordinates:
            ocean_floor[coordinate[1]][coordinate[0]] += 1

    return sum(map(lambda x: sum(y>1 for y in x), ocean_floor))

def exercise_b(vent_coordinates, ocean_floor):
    for coordinates in vent_coordinates:

        # Create array of coordinates from point A to point B
        extended_coordinates = extend_coordinates(coordinates, True)

        # Draw Vents
        for coordinate in extended_coordinates:
            ocean_floor[coordinate[1]][coordinate[0]] += 1

    return sum(map(lambda x: sum(y>1 for y in x), ocean_floor))

def define_ocean_floor(x_coordinate,y_coordinate):

    # Initialize a grid representing the oceansfloor and amount of vents on tile
    return [ [0]*(x_coordinate+1) for i in range(y_coordinate+1)]

def extend_coordinates(coordinates, include_diagonals):
    extended_coordinates = []

    min_x = min([coordinates[0][0], coordinates[1][0]])
    max_x = max([coordinates[0][0], coordinates[1][0]])
    min_y = min([coordinates[0][1], coordinates[1][1]])
    max_y = max([coordinates[0][1], coordinates[1][1]])

    # Vertical
    if coordinates[0][0] == coordinates[1][0]:
        diff = max_y - min_y

        extended_coordinates.append([coordinates[0][0], min_y])
        for i in range(diff):
            offset = 1 + i
            extended_coordinates.append([coordinates[0][0], min_y + offset])

    # Horizontal
    elif coordinates[0][1] == coordinates[1][1]:
        diff = max_x - min_x

        extended_coordinates.append([min_x, coordinates[0][1]])
        for i in range(diff):
            offset = 1 + i
            extended_coordinates.append([min_x + offset, coordinates[0][1]])

    # Diagonally
    elif include_diagonals:
        diff = max_x - min_x

        extended_coordinates.append([coordinates[0][0], coordinates[0][1]])
        for i in range(diff):
            x_coordinate = coordinates[0][0] - (1 + i)  if coordinates[0][0] > min_x else coordinates[0][0] + (1 + i)
            y_coordinate = coordinates[0][1] - (1 + i)  if coordinates[0][1] > min_y else coordinates[0][1] + (1 + i)
            extended_coordinates.append([x_coordinate, y_coordinate])

    return extended_coordinates
