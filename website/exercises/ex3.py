from website import app, parser

def run():
    with app.open_resource('static/input3.txt') as file:
        lines = parser.parseListOfStrings(file.read())
        return { "lead": "Binary Diagnostic",
                 "a": exerciseA(lines),
                 "b": exerciseB(lines)}

def exerciseA(lines):
    gamma  = ""
    epsilon = ""
    for diagnostic in generateMatrix(lines):
        # Check if 0 is the most common of the Nth characters
        bit = "0" if (sum(x=="0" for x in diagnostic) > (len(diagnostic) / 2)) else "1"
        gamma += bit

        # Epsilon is inverted gamma
        epsilon += "0" if bit == "1" else "1"

    return int(gamma, 2) * int(epsilon, 2)

def exerciseB(lines):
    oxygenGenerator = determineCommonality(lines, 'most')
    co2Scrubber = determineCommonality(lines, 'least')
    return oxygenGenerator * co2Scrubber

def determineCommonality(lines, type):
    index = 0
    while len(lines) > 1:
        matrix = generateMatrix(lines)

        if type == 'most':
            # Check if 0 is the most common of the Nth characters
            bit = "0" if (sum(x=="0" for x in matrix[index]) > (len(matrix[index]) / 2)) else "1" 
        else:
            # Check if 0 is the least or tied for least common of the Nth characters
            bit = "0" if (sum(x=="0" for x in matrix[index]) <= (len(matrix[index]) / 2)) else "1"

        lines = list(filter(lambda x: x[index] == bit, lines))
        index += 1
    return int(lines[0],2)

# Generates matrix with lines of the Nth character of each line in the input
def generateMatrix(lines):
    matrix = []
    for index, char in enumerate(lines[0]):

        # Append the Nth character of each line
        matrix.append([x[index] for x in lines])
    return matrix





    

