import numpy as np
from website import app, parser


def run():
    with app.open_resource('static/input8.txt') as file:
        lines = parser.parseSevenSegments(file.read())
        return { "lead": "Seven Segment Search",
                 "a": exercise_a(lines),
                 "b": exercise_b(lines)}

def exercise_a(lines):
    return sum(map(lambda x: count(x[1]) , lines))

def exercise_b(lines):
    return sum(solve_configuration(determine_key(line[0]), line[1]) for line in lines)

def count(output):
    result = 0
    result += sum(map(lambda x: len(x) in {2, 3, 4, 7}, output))
    return result

def solve_configuration(key, output):
    solved_output = ''
    for value in output:
        sorted_value = np.array(list(value))
        sorted_value.sort()

        for index, keys in enumerate(key):
            keys.sort()

            # Match value from output with value from the key
            if np.array_equiv(keys,sorted_value):
                solved_output += str(index)
                break

    return int(solved_output)

def determine_key(pattern):

    pattern_numbers = dict({1:[], 2:[], 3:[], 4:[], 5:[], 6:[], 7:[]})
    solved_pattern = dict({1:'', 2:'', 3:'', 4:'', 5:'', 6:'', 7:''})

    for entry in pattern:
        index = len(entry)
        pattern_numbers[index].append(entry)

    # Prep lines of known numbers and sets of numbers
    number1 = list(pattern_numbers[2][0])
    number7 = list(pattern_numbers[3][0])
    number8 = list(pattern_numbers[7][0])
    number4 = list(pattern_numbers[4][0])
    number069 = list(map(list, pattern_numbers[6]))
    number235 = list(map(list, pattern_numbers[5]))

    #   example fcdeba edcbag decab adcefg acdfb gdcfb acf fabe fa eacfgbd
    #
    #   To solve line 1: Difference in lines between number 1 and number 7
    #       1=c
    #   2=?       3=?
    #       4=?
    #   5=?       6=?
    #       7=?
    solved_pattern[1] = list(set(number7) - set(number1))[0]

    #   To solve line 3: Difference in lines between number 8
    #                    and
    #                    the one with no lines of number 1
    #                          from the set of numbers with 7 lines
    #       1=c
    #   2=?       3=f
    #       4=?
    #   5=?       6=?
    #       7=?
    for number in number069:
        extended_array = [*number, *number1]
        if not (extended_array.count(number1[0]) == 2 and extended_array.count(number1[1]) == 2):
            solved_pattern[3] = list(set(number8) - set(number))[0]
            break

    #   To solve line 6: Get lines from number 7
    #                       Subtract lines 1 and 3
    #       1=c
    #   2=?       3=f
    #       4=?
    #   5=?       6=a
    #       7=?
    solved_pattern[6] = list(set(number7) - set([solved_pattern[1],solved_pattern[3]]))[0]

    #   To solve line 5: Find number 3 -> the number that includes both lines of number 1
    #                                         from the set of numbers with 5 lines
    #                    Find number 2 -> the number that doesn't include line 6
    #                                         from the set of numbers with 5 lines
    #                    Difference in lines between number 2 and 3
    #       1=c
    #   2=?       3=f
    #       4=?
    #   5=g       6=a
    #       7=?
    for number in number235:
        extended_array = [*number, *number1]
        if (extended_array.count(solved_pattern[3]) == 2 and extended_array.count(solved_pattern[6]) == 2):
            number3 = number
        if (extended_array.count(solved_pattern[3]) == 2 and extended_array.count(solved_pattern[6]) == 1):
            number2 = number
        if (extended_array.count(solved_pattern[3]) == 1 and extended_array.count(solved_pattern[6]) == 2):
            number5 = number

    extended_array = [*number2, *number3]
    solved_pattern[5] = list(filter(lambda x: (extended_array.count(x) == 1 and (x != solved_pattern[6] and x != solved_pattern[3])), extended_array))[0]

    # To solve line 2: Find number 5 -> the number that doesn't include line 3
    #                                         from the set of numbers with 5 lines
    #                  Difference in lines between number 3 and 5
    #       1=c
    #   2=e       3=f
    #       4=?
    #   5=g       6=a
    #       7=?
    extended_array = [*number3, *number5]
    solved_pattern[2] = list(filter(lambda x: (extended_array.count(x) == 1 and (x != solved_pattern[6] and x != solved_pattern[3])), extended_array))[0]

    # To solve line 4: Get lines from number 4
    #                       Subtract lines 2, 3, 6
    #       1=c
    #   2=e       3=f
    #       4=b
    #   5=g       6=a
    #       7=?
    solved_pattern[4] = list(set(number4) - set([solved_pattern[2],solved_pattern[3],solved_pattern[6]]))[0]

    # To solve line 7: Get lines from number 8
    #                  Subtract all lines we already know: 1,2,3,4,5,6
    #       1=c
    #   2=e       3=f
    #       4=b
    #   5=g       6=a
    #       7=d
    solved_pattern[7] = list(set(number8) - set([solved_pattern[1],solved_pattern[2],solved_pattern[3],solved_pattern[4],solved_pattern[5],solved_pattern[6]]))[0]


    # Construct each number with the letters from the pattern
    solved0 = np.array([solved_pattern[1], solved_pattern[3], solved_pattern[6], solved_pattern[7], solved_pattern[5], solved_pattern[2]])
    solved1 = np.array([solved_pattern[3], solved_pattern[6]])
    solved2 = np.array([solved_pattern[1], solved_pattern[3], solved_pattern[4], solved_pattern[5], solved_pattern[7]])
    solved3 = np.array([solved_pattern[1], solved_pattern[3], solved_pattern[4], solved_pattern[6], solved_pattern[7]])
    solved4 = np.array([solved_pattern[2], solved_pattern[4], solved_pattern[3], solved_pattern[6]])
    solved5 = np.array([solved_pattern[1], solved_pattern[2], solved_pattern[4], solved_pattern[6], solved_pattern[7]])
    solved6 = np.array([solved_pattern[1], solved_pattern[2], solved_pattern[4], solved_pattern[5], solved_pattern[6], solved_pattern[7]])
    solved7 = np.array([solved_pattern[1], solved_pattern[3], solved_pattern[6]])
    solved8 = np.array([solved_pattern[1], solved_pattern[2], solved_pattern[3], solved_pattern[4], solved_pattern[5], solved_pattern[6], solved_pattern[7]])
    solved9 = np.array([solved_pattern[1], solved_pattern[2], solved_pattern[3], solved_pattern[4], solved_pattern[6], solved_pattern[7]])

    # Return all solved numbers
    return [solved0,solved1,solved2,solved3,solved4,solved5,solved6,solved7,solved8,solved9]
