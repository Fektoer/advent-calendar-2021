import functools as ft
from website import app, parser


def run():
    with app.open_resource('static/input9.txt') as file:
        matrix = Matrix(parser.parseMatrix(file.read()))
        return {"lead": "Smoke Basin",
                "a": exercise_a(matrix),
                "b": exercise_b(matrix)}


def exercise_a(matrix):
    return matrix.get_risk()


def exercise_b(matrix):
    return matrix.calculate_largest_three_basins()


class Matrix():
    def __init__(self, matrix):
        self.matrix = matrix
        self.max_rows = len(matrix)
        self.max_columns = len(matrix[0])
        self.basin = []

    def get_risk(self):
        return sum(x[0]+1 for x in self.get_low_points())

    def get_low_points(self):
        low_points = []
        for idx_ver, row in enumerate(self.matrix):
            for idx_hor, cell in enumerate(row):
                if not sum(x[0] <= cell for x in self.calculate_neighbours(idx_hor, idx_ver)):
                    low_points.append([cell, idx_hor, idx_ver])
        return low_points

    def calculate_largest_three_basins(self):
        basin_sizes = list(self.get_basins())
        basin_sizes.sort(reverse=True)
        return ft.reduce(lambda x, y: x*y, basin_sizes[:3])

    def get_basins(self):
        basin_sizes = []
        low_points = self.get_low_points()

        for point in low_points:
            self.basin = [point]
            self.calculate_basin(point)
            basin_sizes.append(len(self.basin))
        return basin_sizes

    def calculate_basin(self, point, points_visited=None):
        if points_visited is None:
            points_visited = []
        eligible_neighbours = list(filter(lambda x: x[0] != 9, self.calculate_neighbours(point[1], point[2], points_visited)))
        self.basin.extend(eligible_neighbours)

        for neighbour in eligible_neighbours:
            self.calculate_basin(neighbour, self.basin)

    def calculate_neighbours(self, idx_hor, idx_ver, points_visited=None):
        if points_visited is None:
            points_visited = []
        neighbours = []
        if idx_hor - 1 >= 0:
            neighbours.append([self.matrix[idx_ver][idx_hor-1], idx_hor-1, idx_ver])
        if idx_hor + 1 < self.max_columns:
            neighbours.append([self.matrix[idx_ver][idx_hor+1], idx_hor+1, idx_ver])
        if idx_ver - 1 >= 0:
            neighbours.append([self.matrix[idx_ver-1][idx_hor], idx_hor, idx_ver-1])
        if idx_ver + 1 < self.max_rows:
            neighbours.append([self.matrix[idx_ver+1][idx_hor], idx_hor, idx_ver+1])

        # Return only the neighbours not yet visited
        return list(filter(lambda neighbour: not sum(neighbour[1] == x[1] and neighbour[2] == x[2] for x in points_visited), neighbours))
