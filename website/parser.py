def parseListOfIntegers(content):
    return list(content.decode().replace("\r", "").split("\n"))


def parseListOfStrings(content):
    return list(content.decode().replace("\r", "").split("\n"))


def parseSubmarineCommands(content):
    parsedList = []
    commands = list(content.decode().replace("\r", "").split("\n"))
    for command in commands:
        [direction, value] = command.split()
        value = int(value)

        # Up = inverted down, so replace 'up' with 'down' and invert the value for #easylife
        parsedList.append(['down' if direction == 'up' else direction,
                          value*-1 if direction == 'up' else value])
    return parsedList


def parseVents(content):
    coordinatesList = []
    x = []
    y = []
    vents = list(map(lambda x: x.split(" -> "),
                 list(content.decode().replace("\r", "").split("\n"))))
    for vent in vents:
        [x1, y1] = vent[0].split(',')
        [x2, y2] = vent[1].split(',')

        x1 = int(x1)
        x2 = int(x2)
        y1 = int(y1)
        y2 = int(y2)

        x.extend([x1, x2])
        y.extend([y1, y2])
        coordinatesList.append([[x1, y1], [x2, y2]])

    return { "list": coordinatesList, "maxX": max(x), "maxY": max(y) } 

def parseLineOfIntegers(content):
    return list(map(int, list(content.decode().replace("\r", "").split(","))))

def parseSevenSegments(content):

    entries = []
    for entry in list(content.decode().replace("\r", "").split("\n")):
        [pattern, output] = entry.split(" | ")
        entries.append([pattern.split(),output.split()])
    return entries

def parseMatrix(content):
    matrix = []
    for row in list(content.decode().replace("\r", "").split("\n")):
        cells = list(map(int, list(row)))
        matrix.append(cells)
    return matrix

