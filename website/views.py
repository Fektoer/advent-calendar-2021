from flask import Blueprint, render_template, url_for
from website import exercises as exercise

views = Blueprint('views', __name__)

@views.route('/')
def home():
    return render_template('home.html')    

@views.route('/exercise/<id>')
def run(id):
    result = exercise.run(id)
    return render_template('exercise.html', exercise = id, result = result )    

    
